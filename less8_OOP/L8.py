# class Human:
#     age = 20
#     weight = 80
#     height = 1.9
#     student = True
#     sex = "мужчина"

#     def sleep(self):
#         print("Студент спит.")

#     def eat(self):
#         print("Студент ест.")

#     def about(self):
#         print(f"Я {self.sex} в возрасте {self.age} лет. Я ростом {self.height} и весом {self.weight}")

#     def __func(self):
#         print(type(self))
#         print(f"Этот объект сделан из класса {self.__class__}")

# student1 = Human()
# student2 = Human()

# student1._Human__func()

# student2.about()
# student1.weight = 60
# student2.height = 2.3
# student1.about()
# student2.about()


# class Father:
#     smart = True
#     lazy = True

#     def angry(self):
#         print("Резко рассердился.")

# class son(Father):
#     smart = False
#     def boring(self):
#         print("Сыну скучно.")

# class son_of_son(son):
#     pass

# Vasya = Father()

# Misha = son()

# Vasya.angry()
# Misha.boring()
    


# class Cat:
#     def info(self):
#         print("Я кот.")
#     def voice(self):
#         print("Мииаау")

# class Dog:
#     def info(self):
#         print("Я собакен.")
#     def voice(self):
#         print("Гггггаауу")

# Murka = Cat()
# Sharik = Dog()

# for animal in (Murka, Sharik):
#     animal.voice()
#     animal.info()
#     animal.voice()

# print(1 + 1)
# print("1" + "1")


# class Person:
    
#     def __init__(self, name, age, username, password):
#         self.name = name
#         self.age = age
#         self.username = username
#         self.password = password
#         print("Экземпляр создан")
    
#     def about(self):
#         print(self.name,self.age)
#     def __repr__(self):
#         return self.name
#     def __str__(self):
#         return "Это класс, создающий пользователя"

# class User(Person):
#     def __init__(self,name, age, username, password,email):
#         super().__init__(name,age,username,password)
#         self.email = email

# user1 = User("Kolya",25,"KoLy@Nbl4","QWERTY1234","lala@mail.com")








# User1 = Person("Kolya",25,"KoLy@Nbl4","QWERTY1234")
# User2 = Person("Vas",17,"Vasyan","123456765")
# User1.about()
# User2.about()



from abc import ABC, abstractmethod

class ChessPiece(ABC):
    def draw(self):
        print("Drawed")
    
    @abstractmethod
    def move(self):
        pass

class Horse(ChessPiece):
    def move(self):
        print("Пошёл буквой ГЭ")


Figure = Horse()
Figure.move()


    
    